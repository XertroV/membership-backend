```sh
 $ git remote add upstream git@gitlab.com:voteflux/membership-backend.git
 $ git remote set-url --push upstream no_push
 $ git remote add origin git@gitlab.com:xertrov/membership-backend.git
 $ git remote -v
origin	git@gitlab.com:xertrov/membership-backend.git (fetch)
origin	git@gitlab.com:xertrov/membership-backend.git (push)
upstream	git@gitlab.com:voteflux/membership-backend.git (fetch)
upstream	no_push (push)
```

Use git flow where possible. Not strictly enforced but it's nice.
