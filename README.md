# Flux Membership Backend

License: MIT (at the moment)

## About

This contains the core of the Flux membership backend. 
It will be designed to be highly extensible, modular, easy to contribute to, and secure.

